// console.log("Hello World");



/*[SECTION] Objects 
syntax: let objectName = {
	keyA: valueA,
	keyB: valueB
}*/


let cellphone = {
	name: "Nokia 3210", 
	manufacturedDate: 1999
}

console.log("Result from creating objects using initializers/literal notation");
console.log(cellphone);
console.log(typeof cellphone);




/*"this" inside an object*/
function Laptop (name, manufacturedDate){
	this.name = name;
	this.manufacturedDate = manufacturedDate;
}
let laptop = new Laptop("Lenovo", 2008);
console.log("Result from creating objects using object constructors - NEW");
console.log(laptop)


// reusing laptop function
let myLaptop = new Laptop("Macbook Air", 2020);
console.log("Result from creating objects using object constructors - NEW");
console.log(myLaptop)

let oldLaptop = Laptop("Portal R2E CCMC", 1980)
console.log("Result from creating objects using object constructors - NO NEW = undefined");
console.log(oldLaptop)


// Creating empty object
let computer = {}
let myComputer = new Object ();


// [SECTION] ACCESSING OBJECT PROPERTIES
// DOT NOTATION --- to access particular 
console.log("Result from DOT notation: " + myLaptop.name);
console.log("Result from DOT notation: " + myLaptop.manufacturedDate);

// Square bracket notation
console.log("Result from SQUARE BRACKET notation: " + myLaptop['name']);

// Accessing array objects
let array = [laptop, myLaptop];
console.log(array[0]['name']);
console.log(array[0].name);


// [SECTION] INITIALIZING, ADDING and DELETING, RE-ASSIGNING OBJECT PROPERTIES


let car = {};
console.log(car);
// hoisting --> reading a property ahead of its initialization
car.name = "Honda Civic";
console.log('Result from adding a property using dot notation:');
console.log(car);

/*car.manufacturedDate = "2019";
console.log('Result from adding a property using dot notation:');
console.log(car);*/


car['manufacturedDate'] = 2019;
console.log(car.manufacturedDate);
console.log('Result from adding a property using dot notation:');
console.log(car);


// Delete object property
delete car['manufacturedDate'];
console.log("Result from deleting properties:")
console.log(car);

// Reassigning object properties -- modifying/updating
car.name = "Dodge Charger R/T";
console.log('Result from re-assigning properties');
console.log(car);


// [SECTION] OBJECT METHODS
// is a function wich is a property of an object
let person = {
	name: "Cely",
	talk: function(){
		console.log('Hello my name is ' + this.name);
	}
}
console.log(person);
console.log('Result from object method');
person.talk();



// Add a property with an object method/function
person.walk = function(){
	console.log(this.name + " walked 25 steps forward.");
}
person.walk();


// Another example, nested properties inside an object
let friend = {
	firstName: "Joe",
	lastName: "Smith",
	address: {
		city: "Austin",
		country: "Texas"
	},
	emails: ["joe@mail.com", "joesmith@mail.xyz"],
	introduce: function(){
		console.log("Hello, my name is " + this.firstName + " " + this.lastName);
	}
}

friend.introduce();
// [SECTION] Real World Application of Objects

let myPokemon = {
	name: "Pikachu",
	level: 3,
	health: 100,
	attack: 50,
	tackle: function(){
		console.log("This pokemon tackled targetPokemon");
		console.log("targetPokemon's health is now reduced to _targetPokemonHealth_")
	},
	faint: function () {
		console.log("pokemon fainted");
	}
}
console.log(myPokemon);


// creating an object constructor to us THIS and NEW
function Pokemon(name, level){
	// properties
	this.name = name;
	this.level = level;
	this.health = 2 * level;
	this.attack = level;

	// methods/function
	this.tackle = function(target){
		console.log(this.name + " tackled " + target.name);
		console.log("targetPokemon's health is now _targetPokemonHealth_");

	this.faint = function(){
		console.log(this.name + "fainted");
	}
	}
}
let pikachu = new Pokemon("Pikachu", 16);
let rattata = new Pokemon("Rattata", 8);
pikachu.tackle(rattata);


